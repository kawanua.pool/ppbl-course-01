# PPBL Week 5
### Extending Redeemer

## Desired game:

## Suppose that:
- Parameters define game, yielding a unique contract address for each Game
- "Host" can set some parameters like "time" or "price to guess", but should not probably not be allowed to set the "winning number"
- In Datum, we can store a record of previous guesses.
- We also have the option of storing prior guesses in transaction metadata

## Therefore:
- It would be nice to do more with the Redeemer

## Additional notes
From `/src/ParameterizedGame.hs` (live coding 2022-03-16)

### Extending Redeemers
- hi i am thinking like how to write reedemer with more data say integer to write a transaction in frontend part
- Can you write a Redeemer so that the "player" can say "I want Guess #" rather than simply "I want to Guess"?

### Potential use of datum
- Datum: some identifier that says "These UTxOs belong to THIS game" + instructions on how to retreive, how to validate/redeem
- Another game instance would have another Datum
- What functions could I use to look at "all of the UTxOs with THIS datum hash"?

### Public nature of datum
- Datum "somehow should be public" - agree / disagree?
- Blockfrost, for example provides the endpoint:
- https://docs.blockfrost.io/#tag/Cardano-Scripts/paths/~1scripts~1{script_hash}~1redeemers/get

### Would you consider this "on-chain" or "off-chain"?
- What would some sort of "Start Game" endpoint need to do?

### A way to think about parameterized functions:
- Parameterized Validator code is a "bucket" and it defines some rules
- We can reuse the rules
- ...to create unique instance of "bucket"

### Thinking critically about the game we are designing
- Is this game fair?
- Is there a way to prove that it's fair?
- Time limit? Does the host have to do something, or wait for something before ending a game + collecting funds?
- Our game will not be fair if the host knows their answer. They can go win their own game any time.
