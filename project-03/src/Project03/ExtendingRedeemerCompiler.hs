{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Project03.ExtendingRedeemerCompiler
    ( writeScriptGame02
    ) where

import Cardano.Api
import Cardano.Api.Shelley (PlutusScript (..))
import Codec.Serialise (serialise)
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString.Short as SBS
import qualified Ledger

import           Project03.ExtendingRedeemer

writeValidator :: FilePath -> Ledger.Validator -> IO (Either (FileError ()) ())
writeValidator file = writeFileTextEnvelope @(PlutusScript PlutusScriptV1) file Nothing . PlutusScriptSerialised . SBS.toShort . LBS.toStrict . serialise . Ledger.unValidatorScript

writeScriptGame02 :: IO (Either (FileError ()) ())
writeScriptGame02 = writeValidator "output/parameterized-game-02.plutus" $ Project03.ExtendingRedeemer.validator $ GameParam
  {
    gameHost = Ledger.PaymentPubKeyHash "22117fbd0f86a213ae4f4d824cd0d38eea29e49764ae22f5f50ba3d3"
  , answer = 1000
  }
